# Base Error

```javascript
require('zrim-errors').BaseError
```

Inherits Error

## Introduction

Base error class.

## Constructor

The constructor allow you to pass different parameters

```
new BaseError(message : String, options = undefined : Object, cause = undefined : Error)
```

```
new BaseError(options : Object, cause = undefined : Error)
```

Options:
- message *String* : A message
- errorNumber *Number* : A possible error number
- cause *Error* : The root cause

## Properties

### name

Type : `String`

The error name

### message

Type : `String`

Returns the error message

See `getMessage()`

### errorNumber

Type : `Number`

The error number

### cause

Type `Error`

The cause for this error

See `getCause()`

## Methods

### getMessage

```
getMessage() : String
```

Returns the error message

### getCause

```
getCause() : Error
```

Returns the cause for this error

### getDefaultMessage

```
getDefaultMessage() : String
```

Returns the default error message

