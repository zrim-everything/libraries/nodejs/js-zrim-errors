# Security Errors

```javascript
require('zrim-errors').security
```

Contains security errors

## List of errors

- SecurityError : Main security error
- AuthorizationExpiredError : The authorization expired
- AuthenticationRequiredError : An authentication is required
- AccessForbiddenError : The access is forbidden
