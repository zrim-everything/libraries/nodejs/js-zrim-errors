# Common Errors

```javascript
require('zrim-errors').common
```

Contains common errors

## List of errors

- IllegalArgumentError : Inform a given argument is invalid
- ConflictDetectedError : A conflict has been detected
- IllegalStateError : The current state do not permit to do an action
- NotReadyError : Inherits from IllegalStateError and inform the state is not ready
- NotFoundError : Element not found
- NotImplementedError : There is no implementation for the action
- OperationNotPermittedError : The operation is not permitted
- RequirementsNotSatisfiedError : Requirements are not satisfied
- TimedOutError : Action timed out
- AlreadyExistsError : Already exists error
- MismatchError : Mismatch error
- ReadOnlyError : Read only error
- NotAvailableError : Value not available
- NotChangedError : Data not changed
