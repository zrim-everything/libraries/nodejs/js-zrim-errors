import {classUtils} from "zrim-utils";

export namespace baseTypeError {

  export interface ConstructorOptions {
    /**
     * The error message
     */
    message?: string | undefined;

    /**
     * The cause
     */
    cause?: Error | undefined;

    /**
     * The error number
     */
    errorNumber?: number | undefined;
  }

  export interface Properties {
    /**
     * The class name
     */
    className: string;
    /**
     * The error message
     */
    message: string | undefined;
    /**
     * The error number
     */
    errorNumber: number;

    /**
     * The cause
     */
    cause?: Error | undefined;
  }
}


/**
 * Base type error class
 */
export class BaseTypeError extends TypeError {

  /**
   * The properties
   */
  protected properties: baseTypeError.Properties;

  constructor(arg0?: string | baseTypeError.ConstructorOptions | undefined | null, cause?: Error) {
    super();

    const properties = {
      className: classUtils.getClassName(this),
      message: undefined,
      errorNumber: -1
    };

    Object.defineProperty(this, "properties", {
      value: properties,
      enumerable: false,
      writable: true
    });

    this._initDefaultProperties();

    let options;
    if (!arg0) {
      options = {
        message: undefined,
        cause,
        errorNumber: -1
      };
    } else if (typeof arg0 === 'string') {
      options = {
        message: arg0,
        cause,
        errorNumber: -1
      };
    } else {
      options = arg0;
      if (cause) {
        options.cause = cause;
      }
    }

    this._initFromConstructor(options);
  }

  /**
   * The error name
   */
  public get name(): string {
    return this.properties.className;
  }

  /**
   * The error message
   */
  public get message(): string {
    return this.getMessage();
  }

  /**
   * The error number
   */
  public get errorNumber(): number {
    return this.properties.errorNumber;
  }

  /**
   * The cause from this error
   */
  public get cause(): Error | undefined {
    return this.getCause();
  }

  /**
   * Returns the error message
   */
  public getMessage(): string {
    if (this.properties.message && this.properties.message.length > 0) {
      return this.properties.message;
    }

    const cause = this.getCause();
    let message;
    if (cause) {
      if (typeof cause['getMessage'] === 'function') {
        message = cause['getMessage']();
      } else {
        message = cause.message;
      }
    }

    return message || this.getDefaultMessage();
  }

  /**
   * Returns the cause
   */
  public getCause(): Error | undefined {
    return this.properties.cause;
  }

  /**
   * Returns the default message
   */
  public getDefaultMessage(): string {
    return "TypeError";
  }

  /**
   * @inheritDoc
   */
  public toString() {
    return `${this.name}:${this.message}`;
  }

  /**
   * Called to initialize the default internal properties
   */
  protected _initDefaultProperties(): void {

  }

  /**
   * Initialize the instance
   * @param options
   */
  protected _initFromConstructor(options: baseTypeError.ConstructorOptions): void {
    this.properties.message = options.message;
    this.properties.errorNumber = typeof options.errorNumber === 'number' ? options.errorNumber : -1;
    this.properties.cause = options.cause;
  }
}
