import {BaseError} from "./base-error";

export class SecurityError extends BaseError { }
export class AuthorizationExpiredError extends SecurityError { }
export class AuthenticationRequiredError extends SecurityError { }
export class AccessForbiddenError extends SecurityError { }

