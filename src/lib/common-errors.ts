import {BaseError} from './base-error';
import {BaseTypeError} from './base-type-error';

export class IllegalArgumentError extends BaseTypeError { }
export class ConflictDetectedError extends BaseError { }
export class IllegalStateError extends BaseError { }
export class NotReadyError extends IllegalStateError { }
export class NotFoundError extends BaseError { }
export class AlreadyExistsError extends BaseError { }
export class NotImplementedError extends BaseError { }
export class OperationNotPermittedError extends BaseError { }
export class RequirementsNotSatisfiedError extends NotFoundError { }
export class TimedOutError extends BaseError { }
export class MismatchError extends BaseError { }
export class ReadOnlyError extends BaseError { }
export class NotAvailableError extends BaseError { }
export class NotChangedError extends BaseError { }

