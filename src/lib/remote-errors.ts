import {BaseError} from "./base-error";

export class RemoteError extends BaseError { }
export class RemoteServiceError extends RemoteError { }
export class UnavailableRemoteServiceError extends RemoteError { }

